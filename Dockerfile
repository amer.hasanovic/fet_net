FROM registry.gitlab.com/amer.hasanovic/fet_base:latest

SHELL ["/bin/bash","-c","-l"]

ADD cloonix_bin.tar.gz /
ADD demos.tar.gz /demos
ADD bulk.tar.gz /

RUN chmod -R 777 /opt1/cloonix*

RUN apt-get update && apt-get upgrade -y  && \
    DEBIAN_FRONTEND=noninteractive apt-get -y install wireshark-gtk
